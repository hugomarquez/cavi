import SplashScene from '../scenes/splash/SplashScene';
import PrivacyScene from '../scenes/privacy/PrivacyScene';
import WizardScene from '../scenes/wizard/WizardScene';
import PersonalForm from '../scenes/personal/PersonalForm';
import EconomicForm from '../scenes/economic/EconomicForm';
import FamilyForm from '../scenes/family/FamilyForm';
import DaycareForm from '../scenes/daycare/DaycareForm';
import ICETForm from '../scenes/icet/ICETForm';
import Thanks from '../scenes/thanks/ThanksScene';
import ThanksScene from '../scenes/thanks/ThanksScene';

const routes = {
  Splash: {
    name: 'Splash',
    scene: SplashScene,
  },
  Privacy: {
    name: 'Terms and Conditions',
    scene: PrivacyScene,
  },
  Wizard: {
    name: 'Wizard',
    scene: WizardScene,
  },
  Personal: {
    name: 'Personal Information',
    scene: PersonalForm,
  },
  Economic: {
    name: 'Socioeconomic Study',
    scene: EconomicForm,
  },
  Family: {
    name: 'Familiar Study',
    scene: FamilyForm,
  },
  Daycare: {
    name: 'Daycare Information',
    scene: DaycareForm,
  },
  ICET: {
    name: 'ICET Registration Form',
    scene: ICETForm,
  },
  Thanks: {
    name: 'Thanks',
    scene: ThanksScene,
  },
};

export default routes;
