const colors = {
  default: '#6e6e6e',
  primary: '#7912bb',
  secondary: '#ff9c00',
  white: '#fff',
  black: '#6e6e6e',
  textBlack: '4b4b4b',
  textWhite: '#fff',
  error: 'red',
};

export default colors;
