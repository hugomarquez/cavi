import {StyleSheet} from 'react-native';

import colors from '../../config/colors';

const style = StyleSheet.create({
  cardContainer: {
    flex: 0.1,
    paddingBottom: 30,
  },
  container: {
    flex: 1,
    flexDirection: 'row',
  },
  cardInfo: {
    paddingLeft: 20,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  subtitle: {
    fontSize: 18,
  },
});

export default style;
