import React from 'react';
import {View} from 'react-native';
import {Card, Button} from 'react-native-elements';
import style from './style';

const ButtonTab = (props) => {
  return (
    <Card containerStyle={style.cardContainer}>
      <View style={style.container}>
        <Button
          title={props.title}
          containerStyle={style.btn}
          onPress={props.onPress}
          buttonStyle={style.btn}
        />
      </View>
    </Card>
  );
};

export default ButtonTab;
