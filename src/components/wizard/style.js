import {StyleSheet} from 'react-native';
import colors from '../../config/colors';

const style = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  btnBack: {
    alignSelf: 'flex-start',
    padding: 10,
  },
  btnNext: {
    alignSelf: 'flex-end',
    padding: 10,
  },
});

export default style;
