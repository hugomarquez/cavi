import React from 'react';
import {View, Image} from 'react-native';

import style from './style';

const Header = () => {
  return (
    <View style={style.container}>
      <Image style={style.headerBg} source={require('./header_bg.png')} />
      <Image style={style.logo} source={require('./logo.png')} />
    </View>
  );
};

export default Header;
