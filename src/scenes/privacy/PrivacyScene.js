import React from 'react';
import {View, ScrollView} from 'react-native';
import {Text} from 'react-native-elements';

import Header from '../../components/header/Header';
import ButtonTab from '../../components/buttonTab/ButtonTab';

import i18n from '../../config/i18n';
import style from './style';
import routes from '../../config/routes';

const PrivacyScene = ({navigation}) => {
  const {privacy: t} = i18n.t.scenes;
  return (
    <>
      <Header />
      <View style={style.container}>
        <ScrollView>
          <Text h2 style={style.bold}>
            {t.title}
          </Text>
          <Text style={style.bold}>
            {t.section1.title}
            <Text>{t.section1.subtext}</Text>
          </Text>

          <Text style={style.bold}>
            {t.section1.subsection1.title}
            <Text>{t.section1.subsection1.subtext}</Text>
          </Text>
          <View style={style.meaningCont}>
            <Text style={style.bold}>{t.meaning}</Text>
            <Text>{t.section1.subsection1.meaning}</Text>
          </View>

          <Text style={style.bold}>
            {t.section1.subsection2.title}
            <Text>{t.section1.subsection2.subtext}</Text>
          </Text>
          <View style={style.meaningCont}>
            <Text style={style.bold}>{t.meaning}</Text>
            <Text>{t.section1.subsection2.meaning}</Text>
          </View>

          <Text style={style.bold}>
            {t.section1.subsection3.title}
            <Text>{t.section1.subsection3.subtext}</Text>
          </Text>
          <View style={style.meaningCont}>
            <Text style={style.bold}>{t.meaning}</Text>
            <Text>{t.section1.subsection3.meaning}</Text>
          </View>

          <Text style={style.bold}>
            {t.section1.subsection4.title}
            <Text>{t.section1.subsection4.subtext}</Text>
          </Text>
          <View style={style.meaningCont}>
            <Text style={style.bold}>{t.meaning}</Text>
            <Text>{t.section1.subsection4.meaning}</Text>
          </View>

          <Text style={style.bold}>
            {t.section2.title}
            <Text>{t.section2.subtext}</Text>
          </Text>

          <Text style={style.bold}>
            {t.section2.subsection1.title}
            <Text>{t.section2.subsection1.subtext}</Text>
          </Text>
          <View style={style.meaningCont}>
            <Text style={style.bold}>{t.meaning}</Text>
            <Text>{t.section2.subsection1.meaning}</Text>
          </View>

          <Text style={style.bold}>
            {t.section2.subsection2.title}
            <Text>{t.section2.subsection2.subtext}</Text>
          </Text>
          <View style={style.meaningCont}>
            <Text style={style.bold}>{t.meaning}</Text>
            <Text>{t.section2.subsection2.meaning}</Text>
          </View>

          <Text style={style.bold}>
            {t.section2.subsection3.title}
            <Text>{t.section2.subsection3.subtext}</Text>
          </Text>
          <View style={style.meaningCont}>
            <Text style={style.bold}>{t.meaning}</Text>
            <Text>{t.section2.subsection3.meaning}</Text>
          </View>

          <Text style={style.bold}>
            {t.section3.title}
            <Text>{t.section3.subtext}</Text>
          </Text>
          <View style={style.meaningCont}>
            <Text style={style.bold}>{t.meaning}</Text>
            <Text>{t.section3.meaning}</Text>
          </View>

          <ButtonTab
            title={t.accept}
            onPress={(props) =>
              navigation.navigate(routes.Wizard.name, {...props})
            }
          />
        </ScrollView>
      </View>
    </>
  );
};

export default PrivacyScene;
