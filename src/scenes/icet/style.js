import {StyleSheet} from 'react-native';
import colors from '../../config/colors';

const style = StyleSheet.create({
  container: {
    flex: 1,
  },
  scontainer: {
    flex: 1,
    margin: 5,
  },
  checkbox: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 10,
    fontSize: 18,
  },
  subsection: {
    margin: 10,
  },
  inputError: {
    color: colors.error,
  },
  divider: {
    backgroundColor: colors.secondary,
  },
});

export default style;
