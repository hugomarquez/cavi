import React, {useState} from 'react';
import {ScrollView, View} from 'react-native';
import {Text, Divider, Input, CheckBox} from 'react-native-elements';
import {useFormikContext} from 'formik';
import {Picker} from '@react-native-community/picker';

import ButtonTab from '../../components/buttonTab/ButtonTab';
import style from './style';
import i18n from '../../config/i18n';

const {t} = i18n;

const ICETForm = ({data}) => {
  const {
    handleChange,
    handleSubmit,
    setFieldValue,
    values,
    errors,
  } = useFormikContext();

  const [tallerP, setTallerP] = useState(null);
  const [discapacidadP, setDiscapacidadP] = useState(null);
  const [ultimoNivelP, setUltimoNivelP] = useState(null);
  const [situacionTrabajoP, setSituacionTrabajoP] = useState(null);
  const [ocupacionPrincipalP, setOcupacionPrincipalP] = useState(null);
  const [situacionActualP, setSituacionActualP] = useState(null);
  const [liquidarCursoP, setLiquidarCursoP] = useState(null);

  return (
    <ScrollView style={style.scontainer}>
      <View style={style.subsection}>
        <Divider style={style.divider} />
        <Text h4>{t.scenes.ICETForm.title}</Text>
      </View>
      <Picker
        selectedValue={tallerP}
        onValueChange={(v, i) => {
          setFieldValue('taller', v);
          setTallerP(v);
        }}>
        <Picker.Item label={t.schema.icet.taller} value="" />
        {(() => {
          const ltalleres = [];
          data.forEach((taller) => {
            ltalleres.push(
              <Picker.Item
                label={taller.nombre__c}
                value={taller.Id}
                key={taller.Id}
              />,
            );
          });
          return ltalleres;
        })()}
      </Picker>
      <Picker
        selectedValue={discapacidadP}
        onValueChange={(v, i) => {
          setFieldValue('discapacidad', v);
          setDiscapacidadP(v);
        }}>
        <Picker.Item label={t.schema.icet.discapacidad} value="" />
        <Picker.Item label="Ninguna" value="Ninguna" />
        <Picker.Item label="Visual" value="Visual" />
        <Picker.Item label="Auditiva" value="Auditiva" />
        <Picker.Item label="De Lenguaje" value="De Lenguaje" />
        <Picker.Item
          label="Motriz o Músculoesq."
          value="Motriz o Músculoesq."
        />
        <Picker.Item label="Mental" value="Mental" />
      </Picker>
      <Picker
        selectedValue={ultimoNivelP}
        onValueChange={(v, i) => {
          setFieldValue('i_ultimo_nivel', v);
          setUltimoNivelP(v);
        }}>
        <Picker.Item label={t.schema.icet.i_ultimo_nivel} value="" />
        <Picker.Item label="Ninguna" value="Ninguna" />
        <Picker.Item label="Primaria inconclusa" value="Primaria inconclusa" />
        <Picker.Item label="Primaria terminada" value="Primaria terminada" />
        <Picker.Item
          label="Secundaria inconclusa"
          value="Secundaria inconclusa"
        />
        <Picker.Item
          label="Secundaria terminada"
          value="Secundaria terminada"
        />
        <Picker.Item
          label="Preparatoria inconclusa"
          value="Preparatoria inconclusa"
        />
        <Picker.Item
          label="Preparatoria completa"
          value="Preparatoria terminada"
        />
        <Picker.Item label="Técnica inconclusa" value="Técnica inconclusa" />
        <Picker.Item label="Técnica terminada" value="Técnica terminada" />

        <Picker.Item
          label="Técnico Sup. inconclusa"
          value="Técnico Sup. inconclusa"
        />
        <Picker.Item
          label="Técnico Sup. terminada"
          value="Técnico Sup. terminada"
        />

        <Picker.Item label="Lic. inconclusa" value="Lic. inconclusa" />
        <Picker.Item label="Lic. terminada" value="Lic. terminada" />
        <Picker.Item label="Posgrado" value="Posgrado" />
      </Picker>
      <CheckBox
        title={t.schema.icet.i_trabaja_actualmente}
        checked={values.i_trabaja_actualmente}
        onPress={() => {
          setFieldValue('i_trabaja_actualmente', !values.i_trabaja_actualmente);
        }}
      />
      <Input
        placeholder={t.schema.icet.nombre_empresa}
        errorStyle={style.inputError}
        errorMessage={errors.nombre_empresa}
        onChangeText={handleChange('nombre_empresa')}
        value={values.nombre_empresa}
      />
      <Input
        placeholder={t.schema.icet.direccion_trabajo}
        errorStyle={style.inputError}
        errorMessage={errors.direccion_trabajo}
        onChangeText={handleChange('direccion_trabajo')}
        value={values.direccion_trabajo}
      />
      <Input
        placeholder={t.schema.icet.telefono_empresa}
        errorStyle={style.inputError}
        errorMessage={errors.telefono_empresa}
        onChangeText={handleChange('telefono_empresa')}
        value={values.telefono_empresa}
      />
      <Input
        placeholder={t.schema.icet.antiguedad}
        errorStyle={style.inputError}
        errorMessage={errors.antiguedad}
        onChangeText={handleChange('antiguedad')}
        value={values.antiguedad}
      />
      <Picker
        selectedValue={situacionTrabajoP}
        onValueChange={(v, i) => {
          setFieldValue('situacion_trabajo', v);
          setSituacionTrabajoP(v);
        }}>
        <Picker.Item label={t.schema.icet.situacion_trabajo} value="" />
        <Picker.Item label="No especificado" value="No especificado" />
        <Picker.Item label="Empleado u obrero" value="Empleado u obrero" />
        <Picker.Item label="Jornalero o peón" value="Jornalero o peón" />
        <Picker.Item
          label="Trabajador por su cuenta"
          value="Trabajador por su cuenta"
        />
        <Picker.Item label="Patrón o empresario" value="Patrón o empresario" />
        <Picker.Item
          label="Trabajador Familiar sin pago"
          value="Trabajador Familiar sin pago"
        />
      </Picker>
      <Picker
        selectedValue={ocupacionPrincipalP}
        onValueChange={(v, i) => {
          setFieldValue('ocupacion_principal', v);
          setOcupacionPrincipalP(v);
        }}>
        <Picker.Item label={t.schema.icet.ocupacion_principal} value="" />
        <Picker.Item label="No especificado" value="No especificado" />
        <Picker.Item label="Profesionistas" value="Profesionistas" />
        <Picker.Item label="Técnicos" value="Técnicos" />
        <Picker.Item
          label="Trabajador de la Educación"
          value="Trabajador de la Educación"
        />
        <Picker.Item label="Trabajador del Arte" value="Trabajador del Arte" />
        <Picker.Item
          label="Funcionarios y Directivos"
          value="Funcionarios y Directivos"
        />
        <Picker.Item
          label="Trabajador agropecuario"
          value="Trabajador agropecuario"
        />
        <Picker.Item
          label="Inspectores y Supervisores"
          value="Inspectores y Supervisores"
        />
        <Picker.Item label="Artesanos y Obreros" value="Artesanos y Obreros" />
        <Picker.Item
          label="Op. de Máquina Industrial"
          value="Op. de Máquina Industrial"
        />
        <Picker.Item
          label="Ayudantes y Similares"
          value="Ayudantes y Similares"
        />
        <Picker.Item
          label="Operadores de Transporte"
          value="Operadores de Transporte"
        />
        <Picker.Item label="Oficinista" value="Oficinista" />
        <Picker.Item
          label="Comerciantes y Dependientes"
          value="Comerciantes y Dependientes"
        />
        <Picker.Item
          label="Trabajador Ambulante"
          value="Trabajador Ambulante"
        />
        <Picker.Item
          label="Trabajadores en Servicios Públicos"
          value="Trabajadores en Servicios Públicos"
        />
        <Picker.Item
          label="Trabajadores en Servicios Domésticos"
          value="Trabajadores en Servicios Domésticos"
        />
        <Picker.Item
          label="Protección y Vigilancia"
          value="Protección y Vigilancia"
        />
      </Picker>
      <Picker
        selectedValue={situacionActualP}
        onValueChange={(v, i) => {
          setFieldValue('situacion_actual', v);
          setSituacionActualP(v);
        }}>
        <Picker.Item label={t.schema.icet.situacion_actual} value="" />
        <Picker.Item label="Estudiante" value="Estudiante" />
        <Picker.Item label="Se dedica al hogar" value="Se dedica al hogar" />
        <Picker.Item label="Jubilado/Pensionado" value="Jubilado/Pensionado" />
        <Picker.Item
          label="Incapacitado para trabajar"
          value="Incapacitado para trabajar"
        />
        <Picker.Item label="Otro" value="Otro" />
      </Picker>
      <Input
        placeholder={t.schema.icet.medio_entero}
        errorStyle={style.inputError}
        errorMessage={errors.medio_entero}
        onChangeText={handleChange('medio_entero')}
        value={values.medio_entero}
      />
      <Input
        placeholder={t.schema.icet.razon_icet}
        errorStyle={style.inputError}
        errorMessage={errors.razon_icet}
        onChangeText={handleChange('razon_icet')}
        value={values.razon_icet}
      />
      <Input
        placeholder={t.schema.icet.razon_curso}
        errorStyle={style.inputError}
        errorMessage={errors.razon_curso}
        onChangeText={handleChange('razon_curso')}
        value={values.razon_curso}
      />
      <Picker
        selectedValue={liquidarCursoP}
        onValueChange={(v, i) => {
          setFieldValue('liquidar_curso', v);
          setLiquidarCursoP(v);
        }}>
        <Picker.Item label={t.schema.icet.liquidar_curso} value="" />
        <Picker.Item label="Participante" value="Participante" />
        <Picker.Item label="La Empresa" value="La Empresa" />
        <Picker.Item
          label="Servicio Estatal de Empleo"
          value="Servicio Estatal de Empleo"
        />
        <Picker.Item label="El Plantel" value="El Plantel" />
      </Picker>
      <Input
        placeholder={t.schema.icet.otras_areas}
        errorStyle={style.inputError}
        errorMessage={errors.otras_areas}
        onChangeText={handleChange('otras_areas')}
        value={values.otras_areas}
      />
      <ButtonTab title={t.shared.submit} onPress={() => handleSubmit()} />
    </ScrollView>
  );
};

export default ICETForm;
