import * as yup from 'yup';

import i18n from '../../config/i18n';

const {t} = i18n;

export const initialICETValues = {
  i_trabaja_actualmente: false,
  i_ultimo_nivel: '',
  situacion_actual: '',
  otras_areas: '',
  medio_entero: '',
  razon_curso: '',
  razon_icet: '',
  liquidar_curso: '',
  antiguedad: '',
  direccion_trabajo: '',
  discapacidad: '',
  nombre_empresa: '',
  ocupacion_principal: '',
  otra_razon_curso: '',
  otro_medio_entero: '',
  situacion_trabajo: '',
  telefono_empresa: '',
  taller: '',
};

export const ICETSchema = yup.object({
  i_trabaja_actualmente: yup.bool(),
  i_ultimo_nivel: yup.string().required(t.shared.required),
  situacion_actual: yup.string().required(t.shared.required),
  otras_areas: yup.string(),
  medio_entero: yup.string(),
  razon_curso: yup.string(),
  razon_icet: yup.string(),
  liquidar_curso: yup.string().required(t.shared.required),
  antiguedad: yup.string().max(50),
  direccion_trabajo: yup.string().max(255),
  discapacidad: yup.string().required(t.shared.required),
  nombre_empresa: yup.string().max(80),
  ocupacion_principal: yup.string().required(t.shared.required),
  otra_razon_curso: yup.string(),
  otro_medio_entero: yup.string(),
  situacion_trabajo: yup.string().required(t.shared.required),
  telefono_empresa: yup.string(),
  taller: yup.string(),
});
