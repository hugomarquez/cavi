import * as yup from 'yup';

import i18n from '../../config/i18n';

const {t} = i18n;

export const initialFamilyValues = {
  familiares: [
    {
      donde_trabaja: '',
      padece_discapacidad: false,
      padece_enfermedad: false,
      tipo_discapacidad: '',
      tipo_enfermedad: '',
      edad: '',
      imss: '',
      ingreso_mensual: '',
      nombre: '',
      ocupacion: '',
      parentesco: '',
      ultimo_grado: '',
    },
  ],
};

export const FamilySchema = yup.object().shape({
  familiares: yup.array().of(
    yup.object().shape({
      donde_trabaja: yup
        .string()
        .max(80, t.shared.max)
        .required(t.shared.required),
      padece_discapacidad: yup.bool(),
      padece_enfermedad: yup.bool(),
      tipo_discapacidad: yup.string().max(80, t.shared.max),
      tipo_enfermedad: yup.string().max(80, t.shared.max),
      edad: yup.number().max(100).required(t.shared.required),
      imss: yup.string().max(80),
      ingreso_mensual: yup.number().required(t.shared.required),
      nombre: yup.string().max(80, t.shared.max).required(t.shared.required),
      ocupacion: yup.string().max(80, t.shared.max).required(t.shared.required),
      parentesco: yup
        .string()
        .max(80, t.shared.max)
        .required(t.shared.required),
      ultimo_grado: yup
        .string()
        .max(80, t.shared.max)
        .required(t.shared.required),
    }),
  ),
});
