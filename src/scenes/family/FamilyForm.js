import React from 'react';
import {ScrollView, View} from 'react-native';
import {
  Text,
  Divider,
  Input,
  Card,
  Button,
  Icon,
  CheckBox,
} from 'react-native-elements';
import {useFormikContext, FieldArray} from 'formik';

import ButtonTab from '../../components/buttonTab/ButtonTab';
import colors from '../../config/colors';
import style from './style';
import i18n from '../../config/i18n';

const {t} = i18n;

const FamilyForm = () => {
  const {
    handleChange,
    handleSubmit,
    setFieldValue,
    values,
    errors,
  } = useFormikContext();

  return (
    <ScrollView style={style.scontainer}>
      <View style={style.subsection}>
        <Divider style={style.divider} />
        <Text h4>{t.scenes.familyForm.title}</Text>
      </View>
      <FieldArray
        name="familiares"
        render={(arrayHelpers) => (
          <>
            {values.familiares.map((f, i) => (
              <Card key={i}>
                <Text h5>{t.scenes.familyForm.subtitle + '# ' + (i + 1)}</Text>
                <Button
                  buttonStyle={style.btnRemove}
                  onPress={() => arrayHelpers.remove(i)}
                  icon={
                    <Icon
                      name="trash"
                      type="font-awesome-5"
                      size={16}
                      color={colors.white}
                    />
                  }
                />
                <Input
                  placeholder={t.schema.family.nombre}
                  errorStyle={style.inputError}
                  errorMessage={
                    errors.familiares && errors.familiares[i]
                      ? errors.familiares[i].nombre
                      : ''
                  }
                  onChangeText={handleChange(`familiares.${i}.nombre`)}
                  value={f.nombre}
                />
                <Input
                  placeholder={t.schema.family.parentesco}
                  errorStyle={style.inputError}
                  errorMessage={
                    errors.familiares && errors.familiares[i]
                      ? errors.familiares[i].parentesco
                      : ''
                  }
                  onChangeText={handleChange(`familiares.${i}.parentesco`)}
                  value={f.parentesco}
                />
                <Input
                  placeholder={t.schema.family.edad}
                  errorStyle={style.inputError}
                  errorMessage={
                    errors.familiares && errors.familiares[i]
                      ? errors.familiares[i].edad
                      : ''
                  }
                  onChangeText={handleChange(`familiares.${i}.edad`)}
                  value={f.edad}
                />
                <Input
                  placeholder={t.schema.family.ultimo_grado}
                  errorStyle={style.inputError}
                  errorMessage={
                    errors.familiares && errors.familiares[i]
                      ? errors.familiares[i].ultimo_grado
                      : ''
                  }
                  onChangeText={handleChange(`familiares.${i}.ultimo_grado`)}
                  value={f.ultimo_grado}
                />
                <Input
                  placeholder={t.schema.family.ocupacion}
                  errorStyle={style.inputError}
                  errorMessage={
                    errors.familiares && errors.familiares[i]
                      ? errors.familiares[i].ocupacion
                      : ''
                  }
                  onChangeText={handleChange(`familiares.${i}.ocupacion`)}
                  value={f.ocupacion}
                />
                <Input
                  placeholder={t.schema.family.donde_trabaja}
                  errorStyle={style.inputError}
                  errorMessage={
                    errors.familiares && errors.familiares[i]
                      ? errors.familiares[i].donde_trabaja
                      : ''
                  }
                  onChangeText={handleChange(`familiares.${i}.donde_trabaja`)}
                  value={f.donde_trabaja}
                />
                <Input
                  placeholder={t.schema.family.imss}
                  errorStyle={style.inputError}
                  errorMessage={
                    errors.familiares && errors.familiares[i]
                      ? errors.familiares[i].imss
                      : ''
                  }
                  onChangeText={handleChange(`familiares.${i}.imss`)}
                  value={f.imss}
                />
                <Input
                  placeholder={t.schema.family.ingreso_mensual}
                  errorStyle={style.inputError}
                  errorMessage={
                    errors.familiares && errors.familiares[i]
                      ? errors.familiares[i].ingreso_mensual
                      : ''
                  }
                  onChangeText={handleChange(`familiares.${i}.ingreso_mensual`)}
                  value={f.ingreso_mensual}
                />
                <CheckBox
                  title={t.schema.family.padece_discapacidad}
                  checked={f.padece_discapacidad}
                  onPress={() => {
                    setFieldValue(
                      `familiares.${i}.padece_discapacidad`,
                      !f.padece_discapacidad,
                    );
                  }}
                />
                <Input
                  placeholder={t.schema.family.tipo_discapacidad}
                  errorStyle={style.inputError}
                  errorMessage={
                    errors.familiares && errors.familiares[i]
                      ? errors.familiares[i].tipo_discapacidad
                      : ''
                  }
                  onChangeText={handleChange(`familiares.${i}.tipo_discapacidad`)}
                  value={f.tipo_discapacidad}
                />
                <CheckBox
                  title={t.schema.family.padece_enfermedad}
                  checked={f.padece_enfermedad}
                  onPress={() => {
                    setFieldValue(
                      `familiares.${i}.padece_enfermedad`,
                      !f.padece_enfermedad,
                    );
                  }}
                />
                <Input
                  placeholder={t.schema.family.tipo_enfermedad}
                  errorStyle={style.inputError}
                  errorMessage={
                    errors.familiares && errors.familiares[i]
                      ? errors.familiares[i].tipo_enfermedad
                      : ''
                  }
                  onChangeText={handleChange(`familiares.${i}.tipo_enfermedad`)}
                  value={f.tipo_enfermedad}
                />
              </Card>
            ))}
            <Card>
              <Button
                title={t.scenes.familyForm.add + ' '}
                onPress={() => arrayHelpers.push('')}
                buttonStyle={{backgroundColor: colors.secondary}}
                iconRight
                icon={
                  <Icon
                    name="plus"
                    type="font-awesome-5"
                    size={16}
                    color={colors.white}
                  />
                }
              />
            </Card>
          </>
        )}
      />
      <ButtonTab title={t.shared.next} onPress={() => handleSubmit()} />
    </ScrollView>
  );
};

export default FamilyForm;
