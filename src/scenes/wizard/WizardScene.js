import React from 'react';
import {View} from 'react-native';
import Header from '../../components/header/Header';
import {Wizard, WizardStep} from '../../components/wizard/Wizard';

import PersonalForm from '../personal/PersonalForm';
import {PersonalSchema, initialPersonValues} from '../personal/PersonalSchema';

import EconomicForm from '../economic/EconomicForm';
import {
  EconomicSchema,
  initialEconomicValues,
} from '../economic/EconomicSchema';

import FamilyForm from '../family/FamilyForm';
import {FamilySchema, initialFamilyValues} from '../family/FamilySchema';

import DaycareForm from '../daycare/DaycareForm';
import {DaycareSchema, initialDaycareValues} from '../daycare/DaycareSchema';

import ICETForm from '../icet/ICETForm';
import {ICETSchema, initialICETValues} from '../icet/ICETSchema';

import style from '../personal/style';
import i18n from '../../config/i18n';

const {t} = i18n;

const WizardScene = ({navigation, data}) => {
  return (
    <View style={style.container}>
      <Header />
      <Wizard
        navigation={navigation}
        initialValues={{
          ...initialPersonValues,
          ...initialEconomicValues,
          ...initialFamilyValues,
          ...initialDaycareValues,
          ...initialICETValues,
        }}>
        <WizardStep
          progressCard={{
            progress: t.scenes.personalForm.progressCard.progress,
            step: t.scenes.personalForm.progressCard.step,
            currentStep: t.scenes.personalForm.progressCard.currentStep,
            nextStep: t.scenes.personalForm.progressCard.nextStep,
          }}
          validationSchema={PersonalSchema}>
          <PersonalForm />
        </WizardStep>
        <WizardStep
          progressCard={{
            progress: t.scenes.economicForm.progressCard.progress,
            step: t.scenes.economicForm.progressCard.step,
            currentStep: t.scenes.economicForm.progressCard.currentStep,
            nextStep: t.scenes.economicForm.progressCard.nextStep,
          }}
          validationSchema={EconomicSchema}>
          <EconomicForm />
        </WizardStep>
        <WizardStep
          progressCard={{
            progress: t.scenes.familyForm.progressCard.progress,
            step: t.scenes.familyForm.progressCard.step,
            currentStep: t.scenes.familyForm.progressCard.currentStep,
            nextStep: t.scenes.familyForm.progressCard.nextStep,
          }}
          validationSchema={FamilySchema}>
          <FamilyForm />
        </WizardStep>
        <WizardStep
          progressCard={{
            progress: t.scenes.daycareForm.progressCard.progress,
            step: t.scenes.daycareForm.progressCard.step,
            currentStep: t.scenes.daycareForm.progressCard.currentStep,
            nextStep: t.scenes.daycareForm.progressCard.nextStep,
          }}
          validationSchema={DaycareSchema}>
          <DaycareForm />
        </WizardStep>
        <WizardStep
          progressCard={{
            progress: t.scenes.ICETForm.progressCard.progress,
            step: t.scenes.ICETForm.progressCard.step,
            currentStep: t.scenes.ICETForm.progressCard.currentStep,
            nextStep: t.scenes.ICETForm.progressCard.nextStep,
          }}
          validationSchema={ICETSchema}>
          <ICETForm data={[...data]} />
        </WizardStep>
      </Wizard>
    </View>
  );
};

export default WizardScene;
