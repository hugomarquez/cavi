import * as yup from 'yup';

import i18n from '../../config/i18n';

const {t} = i18n;

export const initialPersonValues = {
  nombre: '',
  apellido_materno: '',
  apellido_paterno: '',
  celular: '',
  ciudad_nacimiento: '',
  correo_electronico: '',
  curp: '',
  domicilio_calle: '',
  domicilio_ciudad: '',
  domicilio_cp: '',
  domicilio_colonia: '',
  domicilio_numero: '',
  fecha_nacimiento: '',
  pais_nacimiento: '',
  estado_nacimiento: '',
  estado_civil: '',
  telefono: '',
  sexo: '',
};

export const PersonalSchema = yup.object({
  nombre: yup.string().max(50, t.shared.max).required(t.shared.required),
  apellido_materno: yup
    .string()
    .max(50, t.shared.max)
    .required(t.shared.required),
  apellido_paterno: yup
    .string()
    .max(50, t.shared.max)
    .required(t.shared.required),
  celular: yup.number().required(t.shared.required),
  ciudad_nacimiento: yup
    .string()
    .max(50, t.shared.max)
    .required(t.shared.required),
  correo_electronico: yup.string().email().required(t.shared.required),
  curp: yup.string().max(18, t.shared.max).required(t.shared.required),
  domicilio_calle: yup
    .string()
    .max(80, t.shared.max)
    .required(t.shared.required),
  domicilio_ciudad: yup
    .string()
    .max(80, t.shared.max)
    .required(t.shared.required),
  domicilio_cp: yup.number().required(t.shared.required),
  domicilio_colonia: yup
    .string()
    .max(80, t.shared.max)
    .required(t.shared.required),
  domicilio_numero: yup
    .string()
    .max(50, t.shared.max)
    .required(t.shared.required),
  fecha_nacimiento: yup.date().required(t.shared.required),
  pais_nacimiento: yup.string().required(t.shared.required),
  estado_nacimiento: yup.string().required(t.shared.required),
  estado_civil: yup.string().required(t.shared.required),
  telefono: yup.number().required(t.shared.required),
  sexo: yup.string().max(10, t.shared.max).required(t.shared.required),
});
