import React, {useState} from 'react';
import {ScrollView, View, Platform, Keyboard} from 'react-native';
import {Text, Divider, Input} from 'react-native-elements';
import {useFormikContext} from 'formik';
import moment from 'moment';
import DateTimePicker from '@react-native-community/datetimepicker';
import {Picker} from '@react-native-community/picker';

import ButtonTab from '../../components/buttonTab/ButtonTab';
import style from './style';
import i18n from '../../config/i18n';

const {t} = i18n;

const PersonalForm = () => {
  const {
    handleChange,
    handleSubmit,
    setFieldValue,
    values,
    errors,
  } = useFormikContext();

  const [date, setDate] = useState(new Date(1598051730000));
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);
  const [paisP, setPaisP] = useState(null);
  const [estadoP, setEstadoP] = useState(null);
  const [estadoCivilP, setEstadoCivilP] = useState(null);
  const [sexoP, setSexoP] = useState(null);

  const DP_onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
    setFieldValue('fecha_nacimiento', moment(currentDate).format('l'));
  };

  const DP_showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    Keyboard.dismiss();
    DP_showMode('date');
  };

  return (
    <ScrollView style={style.scontainer}>
      <View style={style.subsection}>
        <Divider style={style.divider} />
        <Text h4>{t.scenes.personalForm.title}</Text>
      </View>
      <Input
        placeholder={t.schema.person.nombre}
        errorStyle={style.inputError}
        errorMessage={errors.nombre}
        onChangeText={handleChange('nombre')}
        value={values.nombre}
      />
      <Input
        placeholder={t.schema.person.apellido_paterno}
        errorStyle={style.inputError}
        errorMessage={errors.apellido_paterno}
        onChangeText={handleChange('apellido_paterno')}
        value={values.apellido_paterno}
      />
      <Input
        placeholder={t.schema.person.apellido_materno}
        errorStyle={style.inputError}
        errorMessage={errors.apellido_materno}
        onChangeText={handleChange('apellido_materno')}
        value={values.apellido_materno}
      />
      <Input
        placeholder={t.schema.person.fecha_nacimiento}
        errorStyle={style.inputError}
        onFocus={Keyboard.dismiss}
        onTouchStart={showDatepicker}
        errorMessage={errors.fecha_nacimiento}
        onChangeText={handleChange('fecha_nacimiento')}
        value={values.fecha_nacimiento}
      />
      {show && (
        <DateTimePicker value={date} mode="date" onChange={DP_onChange} />
      )}
      <Picker
        selectedValue={paisP}
        onValueChange={(v) => {
          setFieldValue('pais_nacimiento', v);
          setPaisP(v);
        }}>
        <Picker.Item label={t.schema.person.pais_nacimiento} value="" />
        <Picker.Item label="México" value="México" />
      </Picker>
      <Picker
        selectedValue={estadoP}
        onValueChange={(v) => {
          setFieldValue('estado_nacimiento', v);
          setEstadoP(v);
        }}>
        <Picker.Item label={t.schema.person.estado_nacimiento} value="" />
        <Picker.Item label="Aguascalientes" value="Aguascalientes" />
        <Picker.Item label="Baja California Norte" value="Baja California Norte" />
        <Picker.Item label="Baja California Sur" value="Baja California Sur" />
        <Picker.Item label="Campeche" value="Campeche" />
        <Picker.Item label="Chiapas" value="Chiapas" />
        <Picker.Item label="Chihuahua" value="Chihuahua" />
        <Picker.Item label="Coahuila" value="Coahuila" />
        <Picker.Item label="Colima" value="Colima" />
        <Picker.Item label="Distrito Federal" value="Distrito Federal" />
        <Picker.Item label="Durango" value="Durango" />
        <Picker.Item label="Edo. de Mexico" value="Edo. de Mexico" />
        <Picker.Item label="Guanajuato" value="Guanajuato" />
        <Picker.Item label="Hidalgo" value="Hidalgo" />
        <Picker.Item label="Jalisco" value="Jalisco" />
        <Picker.Item label="Michoacan" value="Michoacan" />
        <Picker.Item label="Morelos" value="Morelos" />
        <Picker.Item label="Nayarit" value="Nayarit" />
        <Picker.Item label="Nuevo Leon" value="Nuevo Leon" />
        <Picker.Item label="Puebla" value="Puebla" />
        <Picker.Item label="Queretaro" value="Queretaro" />
        <Picker.Item label="San Luis Potosi" value="San Luis Potosi" />
        <Picker.Item label="Sinaloa" value="Sinaloa" />
        <Picker.Item label="Sonora" value="Sonora" />
        <Picker.Item label="Tabasco" value="Tabasco" />
        <Picker.Item label="Tamaulipas" value="Tamaulipas" />
        <Picker.Item label="Tlaxcala" value="Tlaxcala" />
        <Picker.Item label="Veracruz" value="Veracruz" />
        <Picker.Item label="Yucatan" value="Yucatan" />
        <Picker.Item label="Zacatecas" value="Zacatecas" />
      </Picker>
      <Input
        placeholder={t.schema.person.ciudad_nacimiento}
        errorStyle={style.inputError}
        errorMessage={errors.ciudad_nacimiento}
        onChangeText={handleChange('ciudad_nacimiento')}
        value={values.ciudad_nacimiento}
      />
      <Picker
        selectedValue={estadoCivilP}
        onValueChange={(v) => {
          setFieldValue('estado_civil', v);
          setEstadoCivilP(v);
        }}>
        <Picker.Item label={t.schema.person.estado_civil} value="" />
        <Picker.Item label="Union Libre" value="Union Libre" />
        <Picker.Item label="Divorciada" value="Divorciada" />
        <Picker.Item label="Soltera" value="Soltera" />
        <Picker.Item label="Casada" value="Casada" />
        <Picker.Item label="Viuda" value="Viuda" />
        <Picker.Item label="Separada" value="Separada" />
      </Picker>
      <Input
        placeholder={t.schema.person.telefono}
        errorStyle={style.inputError}
        errorMessage={errors.telefono}
        onChangeText={handleChange('telefono')}
        value={values.telefono}
      />
      <Input
        placeholder={t.schema.person.celular}
        errorStyle={style.inputError}
        errorMessage={errors.celular}
        onChangeText={handleChange('celular')}
        value={values.celular}
      />
      <Input
        placeholder={t.schema.person.correo_electronico}
        errorStyle={style.inputError}
        errorMessage={errors.correo_electronico}
        onChangeText={handleChange('correo_electronico')}
        value={values.correo_electronico}
      />
      <Picker
        selectedValue={sexoP}
        onValueChange={(v) => {
          setFieldValue('sexo', v);
          setSexoP(v);
        }}>
        <Picker.Item label={t.schema.person.sexo} value="" />
        <Picker.Item label="Femenino" value="Femenino" />
        <Picker.Item label="Masculino" value="Masculino" />
      </Picker>
      <Input
        placeholder={t.schema.person.curp}
        errorStyle={style.inputError}
        errorMessage={errors.curp}
        onChangeText={handleChange('curp')}
        value={values.curp}
      />
      <View style={style.subsection}>
        <Divider style={style.divider} />
        <Text h4>{t.scenes.personalForm.subtitle}</Text>
      </View>
      <Input
        placeholder={t.schema.person.domicilio_calle}
        errorStyle={style.inputError}
        errorMessage={errors.domicilio_calle}
        onChangeText={handleChange('domicilio_calle')}
        value={values.domicilio_calle}
      />
      <Input
        placeholder={t.schema.person.domicilio_numero}
        errorStyle={style.inputError}
        errorMessage={errors.domicilio_numero}
        onChangeText={handleChange('domicilio_numero')}
        value={values.domicilio_numero}
      />
      <Input
        placeholder={t.schema.person.domicilio_colonia}
        errorStyle={style.inputError}
        errorMessage={errors.domicilio_colonia}
        onChangeText={handleChange('domicilio_colonia')}
        value={values.domicilio_colonia}
      />
      <Input
        placeholder={t.schema.person.domicilio_ciudad}
        errorStyle={style.inputError}
        errorMessage={errors.domicilio_ciudad}
        onChangeText={handleChange('domicilio_ciudad')}
        value={values.domicilio_ciudad}
      />
      <Input
        placeholder={t.schema.person.domicilio_cp}
        errorStyle={style.inputError}
        errorMessage={errors.domicilio_cp}
        onChangeText={handleChange('domicilio_cp')}
        value={values.domicilio_cp}
      />
      <ButtonTab title={t.shared.next} onPress={() => handleSubmit()} />
    </ScrollView>
  );
};

export default PersonalForm;
