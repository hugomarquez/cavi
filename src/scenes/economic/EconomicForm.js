import React, {useState, useEffect} from 'react';
import {ScrollView, View} from 'react-native';
import {Text, Divider, Input, CheckBox} from 'react-native-elements';
import {useFormikContext} from 'formik';
import {Picker} from '@react-native-community/picker';

import ButtonTab from '../../components/buttonTab/ButtonTab';
import style from './style';
import i18n from '../../config/i18n';

const {t} = i18n;

const EconomicForm = (props) => {
  const {
    handleChange,
    handleSubmit,
    setFieldValue,
    values,
    errors,
  } = useFormikContext();

  const [nombreServMedicoP, setNombreServMedicoP] = useState(null);
  const [tipoAguaPotP, setTipoAguaPotP] = useState(null);
  const [tipoVivP, setTipoVivP] = useState(null);
  const [tipoCombustP, setTipoCombustP] = useState(null);
  const [pisoVivP, setPisoVivP] = useState(null);
  const [techoVivP, setTechoVivP] = useState(null);
  const [murosVivP, setMurosVivP] = useState(null);

  return (
    <ScrollView style={style.scontainer}>
      <View style={style.subsection}>
        <Divider style={style.divider} />
        <Text h4>{t.scenes.economicForm.subtitle1}</Text>
      </View>
      <CheckBox
        title={t.schema.economic.certificado_primaria}
        checked={values.certificado_primaria}
        onPress={() => {
          setFieldValue('certificado_primaria', !values.certificado_primaria);
        }}
      />
      <CheckBox
        title={t.schema.economic.certificado_secundaria}
        checked={values.certificado_secundaria}
        onPress={() => {
          setFieldValue(
            'certificado_secundaria',
            !values.certificado_secundaria,
          );
        }}
      />
      <Input
        placeholder={t.schema.economic.s_ultimo_nivel}
        errorStyle={style.inputError}
        errorMessage={errors.s_ultimo_nivel}
        onChangeText={handleChange('s_ultimo_nivel')}
        value={values.s_ultimo_nivel}
      />
      <View style={style.subsection}>
        <Divider style={style.divider} />
        <Text h4>{t.scenes.economicForm.subtitle2}</Text>
      </View>
      <CheckBox
        title={t.schema.economic.servicio_medico}
        checked={values.servicio_medico}
        onPress={() => {
          setFieldValue('servicio_medico', !values.servicio_medico);
        }}
      />
      <Picker
        selectedValue={nombreServMedicoP}
        onValueChange={(v, i) => {
          setFieldValue('cual_servicio_medico', v);
          setNombreServMedicoP(v);
        }}>
        <Picker.Item label={t.schema.economic.cual_servicio_medico} value="" />
        <Picker.Item label="IMSS" value="IMSS" />
        <Picker.Item label="ISSTE" value="ISSTE" />
        <Picker.Item label="SEGURO POPULAR" value="SEGURO POPULAR" />
        <Picker.Item label="ISSSTE LEON" value="ISSSTE LEON" />
        <Picker.Item
          label="SEGURO DE GASTOS MEDICOS"
          value="SEGURO DE GASTOS MEDICOS"
        />
      </Picker>
      <Input
        placeholder={t.schema.economic.titular_imss}
        errorStyle={style.inputError}
        errorMessage={errors.titular_imss}
        onChangeText={handleChange('titular_imss')}
        value={values.titular_imss}
      />
      <CheckBox
        title={t.schema.economic.padece_enfermedad}
        checked={values.padece_enfermedad}
        onPress={() => {
          setFieldValue('padece_enfermedad', !values.padece_enfermedad);
        }}
      />
      <CheckBox
        title={t.schema.economic.toma_medicamento}
        checked={values.toma_medicamento}
        onPress={() => {
          setFieldValue('toma_medicamento', !values.toma_medicamento);
        }}
      />
      <Input
        placeholder={t.schema.economic.peso}
        errorStyle={style.inputError}
        errorMessage={errors.peso}
        onChangeText={handleChange('peso')}
        value={values.peso}
      />
      <Input
        placeholder={t.schema.economic.altura}
        errorStyle={style.inputError}
        errorMessage={errors.altura}
        onChangeText={handleChange('altura')}
        value={values.altura}
      />
      <CheckBox
        title={t.schema.economic.padece_alergia}
        checked={values.padece_alergia}
        onPress={() => {
          setFieldValue('padece_alergia', !values.padece_alergia);
        }}
      />
      <Input
        placeholder={t.schema.economic.cual_alergia}
        errorStyle={style.inputError}
        errorMessage={errors.cual_alergia}
        onChangeText={handleChange('cual_alergia')}
        value={values.cual_alergia}
      />
      <CheckBox
        title={t.schema.economic.consulta_doctor}
        checked={values.consulta_doctor}
        onPress={() => {
          setFieldValue('consulta_doctor', !values.consulta_doctor);
        }}
      />
      <Input
        placeholder={t.schema.economic.nombre_doctor}
        errorStyle={style.inputError}
        errorMessage={errors.nombre_doctor}
        onChangeText={handleChange('nombre_doctor')}
        value={values.nombre_doctor}
      />
      <Input
        placeholder={t.schema.economic.especialidad_doctor}
        errorStyle={style.inputError}
        errorMessage={errors.especialidad_doctor}
        onChangeText={handleChange('especialidad_doctor')}
        value={values.especialidad_doctor}
      />
      <Input
        placeholder={t.schema.economic.telefono_doctor}
        errorStyle={style.inputError}
        errorMessage={errors.telefono_doctor}
        onChangeText={handleChange('telefono_doctor')}
        value={values.telefono_doctor}
      />
      <View style={style.subsection}>
        <Divider style={style.divider} />
        <Text h4>{i18n.t.scenes.economicForm.subtitle3}</Text>
      </View>
      <Picker
        selectedValue={tipoVivP}
        onValueChange={(v, i) => {
          setFieldValue('tipo_vivienda', v);
          setTipoVivP(v);
        }}>
        <Picker.Item label={t.schema.economic.tipo_vivienda} value="" />
        <Picker.Item label="Propia" value="Propia" />
        <Picker.Item label="Rentada" value="Rentada" />
        <Picker.Item label="Prestada" value="Prestada" />
        <Picker.Item label="Posesionario" value="Posesionario" />
        <Picker.Item label="Familiar" value="Familiar" />
      </Picker>
      <Input
        placeholder={t.schema.economic.cuantas_habitaciones}
        errorStyle={style.inputError}
        errorMessage={errors.cuantas_habitaciones}
        onChangeText={handleChange('cuantas_habitaciones')}
        value={values.cuantas_habitaciones}
      />
      <Input
        placeholder={t.schema.economic.cuantas_personas}
        errorStyle={style.inputError}
        errorMessage={errors.cuantas_personas}
        onChangeText={handleChange('cuantas_personas')}
        value={values.cuantas_personas}
      />
      <Input
        placeholder={t.schema.economic.cuantos_banios}
        errorStyle={style.inputError}
        errorMessage={errors.cuantos_banios}
        onChangeText={handleChange('cuantos_banios')}
        value={values.cuantos_banios}
      />
      <Picker
        selectedValue={pisoVivP}
        onValueChange={(v, i) => {
          setFieldValue('tipo_piso', v);
          setPisoVivP(v);
        }}>
        <Picker.Item label={t.schema.economic.tipo_piso} value="" />
        <Picker.Item label="Tierra" value="Tierra" />
        <Picker.Item label="Cemento" value="Cemento" />
        <Picker.Item label="Laminado" value="Laminado" />
        <Picker.Item label="Linoleum o Vinil" value="Linoleum o Vinil" />
        <Picker.Item label="Mosaico o Vitropiso" value="Mosaico o Vitropiso" />
        <Picker.Item label="Otro" value="Otro" />
      </Picker>
      <Picker
        selectedValue={techoVivP}
        onValueChange={(v, i) => {
          setFieldValue('tipo_techo', v);
          setTechoVivP(v);
        }}>
        <Picker.Item label={t.schema.economic.tipo_techo} value="" />
        <Picker.Item label="Material de desecho" value="Material de desecho" />
        <Picker.Item label="Lámina de cartón" value="Lámina de cartón" />
        <Picker.Item label="Lámina metálica" value="Lámina metálica" />
        <Picker.Item label="Lámina de asbezto" value="Lámina de asbezto" />
        <Picker.Item label="Madera o tejamanil" value="Madera o tejamanil" />
        <Picker.Item label="Teja" value="Teja" />
        <Picker.Item label="Losa de concreto" value="Losa de concreto" />
        <Picker.Item label="Otro" value="Otro" />
      </Picker>
      <Picker
        selectedValue={murosVivP}
        onValueChange={(v, i) => {
          setFieldValue('tipo_muro', v);
          setMurosVivP(v);
        }}>
        <Picker.Item label={t.schema.economic.tipo_muro} value="" />
        <Picker.Item label="Material de desecho" value="Material de desecho" />
        <Picker.Item label="Lámina de cartón" value="Lámina de cartón" />
        <Picker.Item label="Lámina metálica" value="Lámina metálica" />
        <Picker.Item
          label="Carrizo, bambú o palma"
          value="Carrizo, bambú o palma"
        />
        <Picker.Item label="Madera" value="Madera" />
        <Picker.Item label="Block o Concreto" value="Block o concreto" />
        <Picker.Item label="Otro" value="Otro" />
      </Picker>
      <CheckBox
        title={t.schema.economic.servicio_agua}
        checked={values.servicio_agua}
        onPress={() => {
          setFieldValue('servicio_agua', !values.servicio_agua);
        }}
      />
      <Picker
        selectedValue={tipoAguaPotP}
        onValueChange={(v, i) => {
          setFieldValue('tipo_agua', v);
          setTipoAguaPotP(v);
        }}>
        <Picker.Item label={t.schema.economic.tipo_agua} value="" />
        <Picker.Item label="Agua y Drenaje" value="Agua y Drenaje" />
        <Picker.Item label="Pipa" value="Pipa" />
        <Picker.Item label="Otro" value="Otro" />
      </Picker>
      <CheckBox
        title={t.schema.economic.servicio_drenaje}
        checked={values.servicio_drenaje}
        onPress={() => {
          setFieldValue('servicio_drenaje', !values.servicio_drenaje);
        }}
      />
      <CheckBox
        title={t.schema.economic.servicio_electricidad}
        checked={values.servicio_electricidad}
        onPress={() => {
          setFieldValue('servicio_electricidad', !values.servicio_electricidad);
        }}
      />
      <CheckBox
        title={t.schema.economic.combustible_cocinar}
        checked={values.combustible_cocinar}
        onPress={() => {
          setFieldValue('combustible_cocinar', !values.combustible_cocinar);
        }}
      />
      <Picker
        selectedValue={tipoCombustP}
        onValueChange={(v, i) => {
          setFieldValue('tipo_combustible', v);
          setTipoCombustP(v);
        }}>
        <Picker.Item label={t.schema.economic.tipo_combustible} value="" />
        <Picker.Item label="Gas Natural" value="Gas Natural" />
        <Picker.Item label="Gas de Tanque" value="Gas de Tanque" />
        <Picker.Item label="Otro" value="Otro" />
      </Picker>
      <CheckBox
        title={t.schema.economic.computadora_casa}
        checked={values.computadora_casa}
        onPress={() => {
          setFieldValue('computadora_casa', !values.computadora_casa);
        }}
      />
      <CheckBox
        title={t.schema.economic.s_trabaja_actualmente}
        checked={values.s_trabaja_actualmente}
        onPress={() => {
          setFieldValue('s_trabaja_actualmente', !values.s_trabaja_actualmente);
        }}
      />
      <Input
        placeholder={t.schema.economic.edad_embarazo}
        errorStyle={style.inputError}
        errorMessage={errors.edad_embarazo}
        onChangeText={handleChange('edad_embarazo')}
        value={values.edad_embarazo}
      />
      <Input
        placeholder={t.schema.economic.edad_matrimonio}
        errorStyle={style.inputError}
        errorMessage={errors.edad_matrimonio}
        onChangeText={handleChange('edad_matrimonio')}
        value={values.edad_matrimonio}
      />
      <ButtonTab title={t.shared.next} onPress={() => handleSubmit()} />
    </ScrollView>
  );
};

export default EconomicForm;
